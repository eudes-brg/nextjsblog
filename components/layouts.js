import Head from "next/head";
import Image from "next/image";
import Link from "next/link";

import TopHeader from "./topHeader";
import BottomHeader from "./bottomHeader";
import Footer from "./footer";


export default function Layout({children,}) {
    return (
        <>
            <Head>
                <meta name="description" content="Learn how to build a personal website using Next.js"/>
            </Head>
            <header>
                <TopHeader phone="phone" email="exemple@gmail.com"/>
                <BottomHeader />
            </header>
            <main>
                {children}
            </main>
            <Footer />
        </>
    )
}