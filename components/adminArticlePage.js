import React from "react"
import AdminTableArticle from "./adminTableArticle"


class AdminArticlePage extends React.Component {

    render () {
        return (
            <div class="sm:px-6 w-full">
                <div class="px-4 md:px-10 py-4 md:py-7">
                    <div class="flex items-center justify-between">
                        <p tabindex="0" class="focus:outline-none text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800">Articles</p>
                    </div>
                </div>
                <div class="bg-white py-4 md:py-7 px-4 md:px-8 xl:px-10">
                    <div class="sm:flex items-center justify-end">
                        <button onclick="popuphandler(true)" class="focus:ring-2 focus:ring-offset-2 focus:ring-indigo-600 mt-4 sm:mt-0 inline-flex items-start justify-start px-6 py-3 bg-indigo-700 hover:bg-indigo-600 focus:outline-none rounded">
                            <p class="text-sm font-medium leading-none text-white">Ajouter un article</p>
                        </button>
                    </div>
                    <table className="w-full whitespace-nowrap table-fixed  my-4">
                        <thead className="text-center">
                            <td>Titre</td>
                            <td>Apperçu</td>
                            <td>Photo</td>
                            <td>Action</td>
                        </thead>
                        <tbody>
                            
                            <AdminTableArticle titre="ceci est le titre" appercu="ceci est un appercu"  image="ceci est l'image"/>
                        </tbody>
                    </table>
                </div>
            </div>
        
        )
    }
}

export default AdminArticlePage