import React from "react";


class  AdminLayout extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            isDropdownOpen : false,
            itemClick : 'Article',
            dataClick : '',
        }
    }
    render () {
        console.log(this.state.isDropdownOpen)
        return (
            <React.Fragment>
                <div className="grid">
                    <nav aria-label="menu nav" class="bg-white border-b-2 pt-2 md:pt-1 pb-1 px-1 mt-0 h-auto fixed w-full z-20 top-0">

                        <div class="flex flex-wrap items-center">
                            <div class="flex flex-shrink md:w-1/3 justify-center md:justify-start text-white">
                                <a href="#" aria-label="Home">
                                    <span class="text-xl pl-2">
                                        <i class="em em-grinning"></i>
                                    </span>
                                </a>
                            </div>

                            <div class="flex flex-1 md:w-1/3 justify-center md:justify-start text-white px-2">
                                <span class="relative w-full">
                                    <input aria-label="search" type="search" id="search" placeholder="Search" class="w-full bg-gray-500 text-white  transition border-b-2 focus:outline-none focus:border-gray-400 rounded py-3 px-2 pl-10 appearance-none leading-normal"/>
                                    <div className="absolute search-icon top-4 left-3">
                                        <svg class="fill-current pointer-events-none text-slate-800 w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                            <path d="M12.9 14.32a8 8 0 1 1 1.41-1.41l5.35 5.33-1.42 1.42-5.33-5.34zM8 14A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"></path>
                                        </svg>
                                    </div>
                                </span>
                            </div>

                            <div class="flex w-full pt-2 content-center justify-between md:w-1/3 md:justify-end">
                                <ul class="list-reset flex justify-between flex-1 md:flex-none items-center">
                                    <li class="flex-1 md:flex-none md:mr-3">
                                        <a class="inline-block py-2 px-4 text-gray-800 no-underline" href="#">Active</a>
                                    </li>
                                    <li class="flex-1 md:flex-none md:mr-3">
                                        <a class="inline-block text-gray-400 no-underline hover:text-gray-200 hover:text-underline py-2 px-4" href="#">link</a>
                                    </li>
                                    <li class="flex-1 md:flex-none md:mr-3">
                                        <div class="relative inline-block">
                                            <button onClick={()=>this.setState({isDropdownOpen: !this.state.isDropdownOpen})} class="drop-button text-gray-800 py-2 px-2"> 
                                                <span class="pr-2">
                                                    <i class="em em-robot_face"></i>
                                                </span> 
                                                Hi, User 
                                                <svg class="h-3 fill-current inline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                                </svg>
                                            </button>
                                            {
                                                this.state.isDropdownOpen ? (
                                                    <div id="myDropdown" class="dropdownlist absolute bg-amber-500 text-white right-0 mt-3 p-3 overflow-auto z-30 ">
                                                        <input type="text" class="drop-search p-2 text-gray-600" placeholder="Search.." id="myInput" onkeyup="filterDD('myDropdown','myInput')"/>
                                                        <a href="#" class="p-2 hover:bg-gray-800 text-white text-sm no-underline hover:no-underline block"><i class="fa fa-user fa-fw"></i> Profile</a>
                                                        <a href="#" class="p-2 hover:bg-gray-800 text-white text-sm no-underline hover:no-underline block"><i class="fa fa-cog fa-fw"></i> Settings</a>
                                                        <div class="border border-gray-800"></div>
                                                        <a href="#" class="p-2 hover:bg-gray-800 text-white text-sm no-underline hover:no-underline block"><i class="fas fa-sign-out-alt fa-fw"></i> Log Out</a>
                                                    </div>
                                                ):('')
                                            }
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </nav>
                
                    <div className="mt-32 pt-2 md:mt-12">
                        <div className="flex flex-col md:flex-row">
                            <nav aria-label="alternative nav">
                                <div class="bg-amber-500 shadow-xl h-20 fixed bottom-0 md:relative md:h-screen z-10 w-full md:w-48 content-center">

                                    <div class="md:mt-12 md:w-48 md:fixed md:left-0 md:top-0 content-center md:content-start text-left justify-between">
                                        <ul class="list-reset flex flex-row md:flex-col pt-3 md:py-3 px-1 md:px-2 text-center md:text-left">
                                            <li class="mr-3 flex-1">
                                                <button onClick={()=>this.setState({itemClick : 'email et telephone'})} class="block px-2 py-1 md:py-3 pl-1 align-middle text-white no-underline hover:bg-white hover:text-gray-500">
                                                    <span class="pb-1 md:pb-0 text-xs md:text-base block md:inline-block">Email et Téléphone</span>
                                                </button>
                                            </li>
                                            <li class="mr-3 flex-1">
                                                <button onClick={()=>this.setState({itemClick : 'email et telephone'})} class="block px-2 py-1 md:py-3 pl-1 align-middle text-white no-underline hover:bg-white hover:text-gray-500">
                                                    <span class="pb-1 md:pb-0 text-xs md:text-base block md:inline-block">Categorie</span>
                                                </button>
                                            </li>
                                            <li class="mr-3 flex-1">
                                                <button onClick={()=>this.setState({itemClick : 'email et telephone'})} class="block px-2 py-1 md:py-3 pl-1 align-middle text-white no-underline hover:bg-white hover:text-gray-500">
                                                    <span class="pb-1 md:pb-0 text-xs md:text-base block md:inline-block">Articles</span>
                                                </button>
                                            </li>
                                            <li class="mr-3 flex-1">
                                                <button onClick={()=>this.setState({itemClick : 'email et telephone'})} class="block px-2 py-1 md:py-3 pl-0 md:pl-1 align-middle text-white no-underline hover:bg-white hover:text-gray-500">
                                                    <span class="pb-1 md:pb-0 text-xs md:text-base block md:inline-block">Témoignages</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <section className="w-full">
                                <div className="main-content flex-1 bg-gray-100 pb-24 md:pb-5">
                                    <div class="bg-gray-800">
                                        <div class="w-full rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
                                            <h1 class="font-bold pl-2 capitalize">{this.state.itemClick}</h1>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex flex-wrap justify-center px-4 py-4">
                                    listes des Articles
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default AdminLayout