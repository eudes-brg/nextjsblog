import CustomLink from "./customLink"


export default function Footer () {
    return (
        <footer className="text-gray-600 bg-amber-500 ">
            <div className="container px-5 py-10 mx-auto">
                <div className="flex flex-wrap md:text-left text-center justify-around">
                    <div className="lg:w-1/2 md:w-1/2 w-full px-4 flex justify-center">
                        <div>
                            <h2 className="title-font font-medium text-white tracking-widest text-sm mb-3 underline underline-offset-1">Nos liens</h2>
                            <nav className="list-none ">
                                <li>
                                    <CustomLink text='Accueil' location='/' styles='text-white' />
                                </li>
                                <li>
                                    <CustomLink text='A propos/ Marabout SESSOU' location='/about' styles='text-white' />
                                </li>
                                <li>
                                    <CustomLink text='Temoignages' location='/temoignages' styles='text-white' />
                                </li>
                            </nav>
                        </div>
                    </div>
                    <div className="lg:w-1/2 md:w-1/2 w-full px-4 flex justify-center">
                        <div>
                            <h2 className="title-font font-medium text-white tracking-widest text-sm mb-3 underline underline-offset-1">Nos services</h2>
                            <nav className="list-none mb-10">
                                <li>
                                    <CustomLink text='Produits et Services du Marabout Medium SESSOU'  location='/prestation' styles='text-white' />
                                </li>

                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}