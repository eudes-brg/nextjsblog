import React from "react"
import Image from "next/image"
import Medium from "../public/initial/medium.jpg"

class Temoignage extends React.Component {
    constructor(props) {
        super(props)

    }
    
    render () {
        return (
            <div className='grid grid-cols-1 grid-rows-1 p-4 bg-gray-100'>
                <div className='grid lg:grid-cols-4 lg:grid-rows-1 sm:grid-rows-2 lg:justify-items-stretch'>
                    <div className="justify-self-center my-4 relative rounded-full h-24 w-24 flex items-center justify-center">
                        <Image 
                            src={Medium}
                            alt='temoin photo'
                            layout="fill"
                            objectFit="cover"
                            objectPosition={'center'}
                            className='rounded-full'
                        />
                    </div>
                    <div className="lg:col-span-3 lg:justify-items-start px-4">
                        <p className="">
                            TÉMOIGNAGE DE LA VALISE MYSTIQUE DU MARABOUT PAPA KOSSI
                            La richesse est la seule chose qui peut changer ta condition de vie et qui peut amener les gens à te respecter. Ce grand marabout maître PAPA KOSSI a changer ma vie de zéro en héro et je suis très reconnaissant en vers lui en lui témoigner sur son site, et je demande à tous ceux qui ont besoin de le contactez le plus vite possible avant qu’il ne soit trop tard. MERCI
                        </p>
                        <p className="font-bold text-lg my-4">Nom,Pays</p>
                        <p>Profession</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default Temoignage