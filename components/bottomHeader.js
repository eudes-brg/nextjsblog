import { useState } from "react"
import CustomLink from "./customLink"
import Image from "next/image"
import logo from "../public/initial/cristall.jpg"

export default function BottomHeader(params) {
    const [isOpenSearchBar,setIsOpenSearchBar] = useState(false)
    const [isOpenMobileNav,setIsOpenMobileNav] = useState(false)
    const nav = [
        {'location':'/','text':'Accueil'},
        {'location':'/about','text':'A propos'},
        {'location':'/temoignages','text':'Témoignages'},
        {'location':'prestation','text':'Mes services et produits'},
        {'location':'contact','text':'contact-commande'},
        {'location':'#','text':'Confidentialite'}
    ];
    return (
        <nav className="bg-white border-b-2">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div className="flex items-center flex-row justify-between h-20">
                    <div className="flex items-center">
                        <div className="flex-shrink-0">
                            <Image
                            className="h-8 w-8"
                            src={logo}
                            alt="logo"
                            width={55}
                            height={55}
                        />
                        </div>
                    </div>
                    {isOpenSearchBar ? (
                        <div className="w-3/4 flex justify-end">
                            <div className="pt-0 w-3/4">
                                <input type="text" placeholder="Search" className="px-3 py-3 text-gray-800 relative text-base border-0 border-b-2 outline-none focus:outline-none w-full" autoFocus/>
                            </div>
                        </div>
                    ):(
                        <div className="hidden md:block">   
                            <div className="ml-10 flex items-baseline space-x-4">
                                {
                                   nav.map((item,index)=>{
                                       return  (
                                            <CustomLink
                                                key={index}
                                                location={item.location} 
                                                styles="font-semibold text-gray-800 px-3 py-2 rounded-md text-base" 
                                                text={item.text} 
                                            />
                                       )
                                   })
                                }
                            </div>   
                        </div>
                    )}
                    <div className="flex">
                        <div className="flex items-center text-gray-800 mx-2">
                            <button
                                className="inline-flex items-center justify-center p-2 rounded-md text-gray-400"
                                onClick={()=>setIsOpenSearchBar(!isOpenSearchBar)}
                                type="button"
                            >
                                {isOpenSearchBar ? (
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                                    </svg>
                                ):(
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                                    </svg>
                                )}
                                
                                
                            </button>
                        </div>
                        <div className="-mr-2 flex md:hidden mx-2">
                            <button
                                onClick={() => setIsOpenMobileNav(!isOpenMobileNav)}
                                type="button"
                                className="inline-flex items-center justify-center p-2 rounded-md text-gray-400"
                                aria-controls="mobile-menu"
                                aria-expanded="false"
                            >
                                
                                <span className="sr-only">Open main menu</span>
                
                                {isOpenMobileNav ? (
                                    <div>
                                        <svg
                                            className=" block h-6 w-6"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke="currentColor"
                                            aria-hidden="true"
                                        >
                                            <path
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                                strokeWidth="2"
                                                d="M6 18L18 6M6 6l12 12"
                                            />
                                        </svg>
                                    </div>
                                ):(
                                    <div>
                                        <svg
                                            className="block h-6 w-6"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke="currentColor"
                                            aria-hidden="true"
                                        >
                                            <path
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                                strokeWidth="2"
                                                d="M4 6h16M4 12h16M4 18h16"
                                            />
                                        </svg>
                                    </div>
                                )}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
    
            <div className="md:hidden" id="mobile-menu">
                {isOpenMobileNav ? (
                    <div className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
                        {
                            nav.map((item,index)=>{
                                return  (
                                    <CustomLink 
                                        key={index}
                                        location={item.location} 
                                        styles=" block px-3 py-2 rounded-md text-base font-medium" 
                                        text={item.text} 
                                    />
                                )
                            })
                        }
                    </div>
                    ):(
                        ""
                    )}
            </div>
        </nav>
    )
}