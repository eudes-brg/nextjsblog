import React from "react";
import Head from "next/head"
import Image from "next/image";
import Medium from "../public/initial/medium.jpg"
import CustomLink from "../components/customLink";
import Quality from '../public/initial/quality.jpg'
import Resultat from '../public/initial/resultat.jpg'


import Layout from "../components/layouts"

class  About extends React.Component {
  
  render () {

    return (
      <div>
        <Head>
          <title>Apropos-le plus grand maître compétent et puissant du monde papa SESSOU</title>
        </Head>
        <Layout>
          <div className="bg-white my-8">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-4">
                <div className="grid md:grid-cols-2 grid-rows-1 gap-x-8 place-items-center">
                    <div>
                        <Image 
                            src={Medium}
                            alt="medium"
                        />
                    </div>
                    <div className="sm:px-6 lg:px-8">
                        <h1 className="uppercase font-semibold text-2xl text-center mx-6 text-red-600">
                            NE RESTEZ PAS SEUL FACE A VOS ANGOISSES
                        </h1>
                        <p className="text-center text-lg my-8 mx-6 font-semibold">
                            Je vous aide à resoudre vos problèmes même dans les cas les plus désespérés et spéciaux.
                        </p>
                        <p className="text-lg mx-6 ">
                            Travail sérieux et aussi par correspondance. Résultat éfficace et rapide.
                        </p>
                    </div>
                </div>
            </div>
          </div>

          <div className="bg-white my-11">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div className="grid md:grid-cols-2 md:gap-x-11 justify-items-center ">
                    <div className="py-4 my-4">
                        <CustomLink  location="/about" text="Pourquoi ce blog?" styles="p-4 border-2 text-red-600 border-red-500"/>
                    </div>
                    <div className="py-4 my-4">
                        <CustomLink  
                            location="/about" 
                            text="Qui est SESSOU" 
                            styles="p-4 border-2 text-red-600 border-red-500"
                        />
                    </div>
                </div>
                <div className="py-4 my-4">
                    <p className="mb-4">
                        <em>Bienvenu,</em>
                    </p>
                    <p className="mb-4 text-lg">
                        <em>
                            Vous êtes sur le blog principale du 
                            <strong>grand marabout médium SESSOU</strong>
                            , accessible à l’adresse grand-maître-marabout-médium-lokoza.com en tout temps depuis n’importe quel lieu.
                        </em>
                    </p>
                    <p className="mb-4 text-lg">
                        <em>
                            Par ici vous pouvez prendre directement contact avec le 
                            <strong>marabout médium SESSOU</strong>
                            et obtenir de satisfaction en ce qui concerne vos problèmes de pauvreté, de retour d’affection, d’envoûtement et bien d’autre
                        </em>
                    </p>
                </div>
                <div className="py-4 my-4">
                    <p className="mb-4 text-lg">
                        <span>
                            MES  DOMAINES DE COMPETENCE
                        </span>
                    </p>
                    <p className="mb-4 text-lg">
                        En particulier, j’interviens dans presque tous les domaines de la spiritualité en Afrique et partout où mon besoin se faire ressentir dans le monde.
                    </p>
                    <p className="mb-4 text-lg">
                        Mais j’ai mes points forts et les travaux que je fais fréquement. voici la liste d’activité premium:
                    </p>

                    <ul className="mb-4 text-lg">
                        <li>- Sentiments, couple et amour</li>
                        <li>- Séparation et retour de l’être aimé</li>
                        <li>- Retour affectif, faire revenir son ex</li>
                        <li>- Fidélité et harmonie du couple</li>
                        <li>- Protection contre les ennemis</li>
                        <li>- Protection contre les mauvais sorts et le mauvais œil</li>
                        <li>- Protection contre les ennemis et dangers</li>
                        <li>- Dés-envoûtement de lieux et personnes</li>
                        <li>- Chance aux jeux</li>
                        <li>- Guérisseur historique africain</li>
                        <li>- Maladies chroniques</li>
                        <li>- Examens et concours</li>
                        <li>- Résultats rapides</li>
                    </ul>

                    <p className="mb-4 text-lg">
                        Mais n’hésitez pas à me contacter pour n’importe quel besoin que vous avez. Vous pouvez également me demander une formation complète ou une initiation aux pourvois mysthiques des Dieux de l’Afrique
                    </p>
                </div>
                <div className="py-8 border-y-2">
                    <p className="my-4 text-center">Grand Mage SESSOU est à votre disposition 24 H / 24 et 7J / 7J.</p>
                    <p className="my-4 text-center">DISCRÉTION TOTALE GARANTIE</p>
                    <p className="my-4 text-center">MON ENGAGEMENT</p>
                    <div className="my-4 flex justify-center">
                        <Image 
                            src={Quality}
                            alt='engagement'
                        />
                    </div>
                    <p className="my-4 text-center">
                        Je m’engage à faire preuve d’une honnêteté et d’une franchise totale. 
                        Je ne vous cacherai rien de ce que mes dons de voyant m’apprendront sur votre avenir. 
                        Rien de ce que vous me direz où de ce que je découvrirai à travers ma voyance ne sera divulgué. 
                        Notre relation restera strictement confidentielle. 
                        Je m’engage à mettre toute l’étendue de mes connaissances à votre service, qu’il s’agisse de mes dons de marabout ou de ma maîtrise de la voyance.
                    </p>
                    <p className="my-4 text-center">
                        MON RÉSULTAT
                    </p>
                    <div className="my-4 flex justify-center">
                        <Image
                            src={Resultat}
                            alt="resultat"
                        />
                    </div>
                    <div className="">

                    </div>
                </div>
                <div className="grid md:grid-cols-2 grid-rows-1 gap-x-8 place-items-center py-4">
                    <div>
                        <Image 
                            src={Medium}
                            alt="medium"
                        />
                    </div>
                    <div className="sm:px-6 lg:px-8">
                        <p className="mb-4 text-lg">
                            <span>
                                <strong>Devenir riche grâce a la valise magique mystique multiplicateur d’argent</strong>
                            </span>
                        </p>
                        <p className="mb-4 text-base">
                            Depuis des siècles, tous ceux qui possède la valise magique 
                            multiplicateur d’argent ont toujours un niveau financière très élevé 
                            et le renommé international. Ils sont tous riche respecté et ont tous 
                            une réputation de renommé international soit ils sont artiste, 
                            footballeur, politicien ou Multi milliardaire etc….
                            Cependant, beaucoup des gens dans le monde entiers ne savent pas comment 
                            est-ce qu’ils font pour obtenir leurs fortune si remarquable? 
                            Mais pourtant ils sont belle et bien un secret qui n’est rien que 
                            la valise magique multiplicateur d’argent. Ainsi donc, le secret qu’est 
                            LA VALISE MYSTIQUE de la richesse qui a été dévoilé et instaurer 
                            par le plus grand prophète du culte vaudou. Cette valise en question 
                            à le pouvoir de vous rendre riche d’une manière jamais vue sur terre 
                            car vous serrez le maître de l’argent et le dieu du pouvoir financière. 
                            Raison pour laquelle les détenant de la valise magique ne se plaint 
                            pas de la vie et sur le plan financier.
                        </p>
                    </div>
                </div>
                <div className="grid md:grid-cols-2 grid-rows-1 gap-x-8 place-items-center">
                    <div>
                        <Image 
                            src={Medium}
                            alt="medium"
                        />
                    </div>
                    <div className="sm:px-6 lg:px-8">
                        <p className="mb-4 text-lg">
                            <span>
                                <strong>Devenir riche grâce a la valise magique mystique multiplicateur d’argent</strong>
                            </span>
                        </p>
                        <p className="mb-4 text-base">
                            Depuis des siècles, tous ceux qui possède la valise magique 
                            multiplicateur d’argent ont toujours un niveau financière très élevé 
                            et le renommé international. Ils sont tous riche respecté et ont tous 
                            une réputation de renommé international soit ils sont artiste, 
                            footballeur, politicien ou Multi milliardaire etc….
                            Cependant, beaucoup des gens dans le monde entiers ne savent pas comment 
                            est-ce qu’ils font pour obtenir leurs fortune si remarquable? 
                            Mais pourtant ils sont belle et bien un secret qui n’est rien que 
                            la valise magique multiplicateur d’argent. Ainsi donc, le secret qu’est 
                            LA VALISE MYSTIQUE de la richesse qui a été dévoilé et instaurer 
                            par le plus grand prophète du culte vaudou. Cette valise en question 
                            à le pouvoir de vous rendre riche d’une manière jamais vue sur terre 
                            car vous serrez le maître de l’argent et le dieu du pouvoir financière. 
                            Raison pour laquelle les détenant de la valise magique ne se plaint 
                            pas de la vie et sur le plan financier.
                        </p>
                    </div>
                </div>
            </div>
          </div>
        </Layout>
      </div>
    )

  }
}

export default About


