import React from "react";
import Layout from "../components/layouts";

class Confidentialite extends React.Component {
    constructor(props) {
        super(props)
    }

    render () {
        return (
            <Layout>
                <div className="bg-white">
                    <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                        Page de confidentialite
                    </div>
                </div>
            </Layout>
        )
    }
}

export default Confidentialite