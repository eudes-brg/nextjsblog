import React from "react";
import Head from "next/head"
import Image from "next/image";
import Medium from "../public/initial/medium.jpg"
import CustomLink from "../components/customLink";


import Layout from "../components/layouts"
import Temoignage from "../components/temoignage";

class  Temoignages extends React.Component {
  
  render () {

    return (
      <div>
        <Head>
          <title>Temoignages-le plus grand maître compétent et puissant du monde papa SESSOU</title>
        </Head>
        <Layout>
          <div className="bg-white my-8">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-4">
                <div className="grid md:grid-cols-3 grid-rows-1 gap-x-8 place-items-center">
                    <div>
                        <Image 
                            src={Medium}
                            alt="medium"
                        />
                        <p className="font-semibold my-2 uppercase">médium SESSOU</p>
                        <p className="text-gray-500 uppercase">Bénin, Djougou</p>
                    </div>
                    <div className="col-span-2 sm:px-6 lg:px-8 justify-items-center place-content-center place-self-center">
                        <p className="font-semibold my-2 text-3xl text-center">LES TEMOIGNAGES FAITES SUR LE PUISSANT MARABOUT MEDIUM KOSSI</p>
                        <p className="text-justify text-lg">Je continue toujours de faire des efforts pour fournir le meilleurs des produits et services de mes clients. Quand ils sont satisfaits et heureux d’autre n’hésitent pas de partager leurs témoignages avec le reste de ma communauté. Découvrez ci-dessous les meilleurs témoignage de mes clients faites sur ” les retours d’affections”, “le portefeuille magique”, “la valise magique”, “autres rituels”…</p>
                    </div>
                </div>
            </div>
          </div>

          <div className="bg-white my-11">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 ">
                <div className="grid lg:grid-cols-2 sm:grid-cols-1 gap-4 container mx-auto mt-8 justify-items-center border-b-2 pb-16">
                    <div>
                        <p className="font-semibold">LA DIVINATION DU 3eme OEIL</p>
                        <p className="mb-4 mt-2">
                            Comment arriver à percevoir l’avenir et le préparer sans auparavant connaître les éléments du destin ? Par quels moyens aborder les esprits des univers invisibles pour communiquer avec les défunts de l’au-delà ? Ces questions imposent des réponses mystiques…
                        </p>
                        <p className="mb-4">
                            Deviner les lendemains est une pratique réservée aux élus ayant reçu un don de naissance. C’est avec son 3ème œil extrasensoriel que le voyant médium SESSOU peut accéder à l’éther inaccessible pour réaliser ses visions divinatoires et accéder ainsi à la vérité.*
                        </p>
                    </div>
                    <div>
                        <p className="font-semibold">LES PREDICTIONS DU VOYANT MEDIUM</p>
                        <p className="mb-4 mt-2">
                            La voyance nécessite la complète maîtrise des sciences ésotériques pour assurer la connexion entre le monde terrestre et les sphères spirituelles. Alors, le voyant peut établir des prédictions dans le futur d’une personne, d’une famille ou d’une groupe. Les analyses du Pr. KOSSI lui permettent ainsi de faire des révélations et des prémonitions sur les jours de chance, les choix essentiels de la vie, etc…*
                        </p>
                        <p className="mb-4">
                            Le médiumnité est une hyper sensibilité aux spectres des esprits et à leurs influences. Le Médium est pourvu de sens supplémentaires pour détecter et décoder les messages de l’au-delà et autoriser des échanges entre ses clients et leurs relations décédées et disparues.* 
                        </p>
                        <p className="mb-4">
                        Envie de prendre les bonnes décisions ? Besoin de voir le futur et les temps passés ? L’esprit divinatoire du voyant médium KOSSI illumine de réponses fiables et justes tout ceux qui sont désorientés et qui ont besoin d’être guidé par la lumière !.  
                        </p>
                    </div>
                </div>
                <div className='grid grid-cols-1 grid-rows-1 gap-6 container mx-auto'>
                    <Temoignage /> 
                    <Temoignage /> 
                    <Temoignage /> 
                    <Temoignage /> 
                </div>
            </div>
          </div>
        </Layout>
      </div>
    )

  }
}

export default Temoignages


