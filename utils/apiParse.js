
import Parse from 'parse'
import {PARSE_APP_ID,PARSE_JS_KEY,PARSE_SERVER_URL } from "../config"

Parse.initialize(PARSE_APP_ID,PARSE_JS_KEY)
Parse.serverURL = PARSE_SERVER_URL



export async function getEmail () {
    let blogInfo = Parse.Object.extend("blogInfo")
    let querry = new Parse.Query(blogInfo);
    try {
        let data = await querry.first()
        return data
    } catch (error) {
    console.log("Error: " + error.code + " " + error.message); 
    }

}

export function changeEmail(params) {
    
}

export function getPhone(params) {
    
}

export function changePhone () {

}

export function getCategory () {

}

export function createCategory () {

}

export function updateCategory () {

}

export function deleteCategory(params) {
    
}

export async function getArticles () {
    let article = Parse.Object.extend("Article")
    let query = new Parse.Query(article);
    try {
        let data =  await query.findAll()
        return data.reverse()
    } catch (error) {
    console.log("Error: " + error.code + " " + error.message); 
    }
}

export function createArticle(params) {
    
}

export function updateArticle (params) {
    
}

export function deleteArticle (params) {
    
}

export function getCategoryArticle () {

}

export function getTestimony () {

}

export function createTestimony(params) {
    
}

export function updateTestimony(params) {
    
}

export function deleteTestimony(params) {
    
}